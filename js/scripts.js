$(document).ready(function () {
    var checklist

    init();

    // add item below
    $(document).on('click','.add-item-below',function(e){
        var liObj = e.target.parentElement.parentElement
        var index = $("#checklist li").index(liObj);

        var item = setItem('untitled')
        checklist.list.splice((index+1),0,item)
        var listData = JSON.stringify(checklist)
        localStorage.setItem('checklist', listData)

        var itemHtml = renderInputItem()
        $(liObj).after(itemHtml)
    });

    // switch current item type
    $(document).on('click','.change-type',function(e){
        var liObj = e.target.parentElement.parentElement
        var index = $("#checklist li").index(liObj);

        var currentType = checklist.list[index].type
        var updatedType = currentType=='item'?'cate':'item'
        checklist.list[index].type = updatedType
        var listData = JSON.stringify(checklist)
        localStorage.setItem('checklist', listData)

        $(liObj).find('.itemType').removeClass(currentType)
        $(liObj).find('.itemType').addClass(updatedType)
        $(liObj).find('.change-type').html(updatedType)
    });

    // remove current item
    $(document).on('click','.del-item',function(e){
        var liObj = e.target.parentElement.parentElement
        var index = $("#checklist li").index(liObj);

        checklist.list.splice(index,1)
        var listData = JSON.stringify(checklist)
        localStorage.setItem('checklist', listData)
        $(liObj).remove()
    });

    // save insert item
    $(document).on('click','.save-insert-item',function(e){
        var liObj = e.target.parentElement.parentElement
        var index = $("#checklist li").index(liObj);
        var name = $(liObj).find('.title').val()
        checklist.list[index].name = name

        var listData = JSON.stringify(checklist)
        localStorage.setItem('checklist', listData)

        var item = {name:name, type:checklist.list[index].type, checked:checklist.list[index].checked}
        var itemHtml = renderItem(item)
        $(liObj).after(itemHtml)
        $(liObj).remove()
    });

    // remove insert item
    $(document).on('click','.del-insert-item',function(e){
        var liObj = e.target.parentElement.parentElement
        var index = $("#checklist li").index(liObj);
        checklist.list.splice(index,1)
        var listData = JSON.stringify(checklist)
        localStorage.setItem('checklist', listData)
        $(liObj).remove()
    });

    // double click the item/cate
    $(document).on('dblclick','.itemType',function(e){
        var liObj = $(this).parent()
        var index = $("#checklist li").index(liObj);
        var name = $(liObj).find('.form-check-label').html()
        var itemHtml = renderDbclickItem(name)
        $(liObj).after(itemHtml)
        $(liObj).remove()
    });

    // checkbox event
    $(document).on('change','.form-check-input',function(e){
        var liObj = $(this).parent().parent().parent()
        var index = $("#checklist li").index(liObj);

        checklist.list[index].checked = $(this).prop("checked")
        var listData = JSON.stringify(checklist)
        localStorage.setItem('checklist', listData)
    });

    // save new item
    $('.new-item .save-item').click(function () {
        var name = $('.new-item .title').val()
        if(name.length == 0){
            // empty item name
            return $('.alert-block').show();
        }else{
            var item = setItem(name)
            checklist.list.push(item)
            var listData = JSON.stringify(checklist)
            localStorage.setItem('checklist', listData)

            $('.new-item .title').val('')

            var item = setItem(name)
            var itemHtml = renderItem(item)
            $('#checklist').append(itemHtml)
        }
    });

    // hide alert
    $('.alert-block .close').click(function () {
        $('.alert-block').hide();
    });

    // Open Name of Checklist Modal
    $('#openChecklistModalButton').click(function () {
        $("#newChecklistModal").modal('show');
    });

    // input checklist name
    $('#checklistName').bind('input', function () {
        $('.alert-text').hide();
    });

    // create checklist
    $('.createChecklist').click(function () {
        var name = $('#checklistName').val();
        if(name.length==0){
            $('.alert-text').show();
        }else{
            checklist = {name: name, status:'inactive', list:[]}
            localStorage.setItem('checklist', JSON.stringify(checklist));

            init()
            $("#newChecklistModal").modal('hide');
        }
    });

    // save checklist
    $('#saveChecklistButton').click(function () {
        $('.checklist-block').removeClass('inactive');
        $('.checklist-block').addClass('active');
        checklist.status = 'active';
        localStorage.setItem('checklist', JSON.stringify(checklist));
    });

    // edit checklist
    $('#editChecklistButton').click(function () {
        $('.checklist-block').removeClass('active');
        $('.checklist-block').addClass('inactive');
        checklist.status = 'inactive';
        localStorage.setItem('checklist', JSON.stringify(checklist));
    });

    function init(){
        checklist = localStorage.getItem('checklist')
        console.log('checklist ==>'+checklist)
        if(!checklist){
            $('.checklist-block .empty-block').show();
            $('.checklist-block .list-block').hide();
        }else{
            $('.checklist-block .empty-block').hide();
            checklist = JSON.parse(checklist)
            displayChecklist();
        }
    }

    function displayChecklist() {
        $('.checklist-block .list-block h4').html(checklist.name);

        checklist.list.forEach( function (item, index) {
            var itemHtml = renderItem(item);
            $('#checklist').append(itemHtml);
        });

        $('.checklist-block').addClass(checklist.status);

        $('.checklist-block .list-block').show();
    }

    function renderItem(item){
        var checked = item.checked?'checked':''
        var itemHtml = '<li class="d-flex li-'+item.type+' justify-content-between align-items-center col-12 col-md-10">\n' +
            '                                    <div class="itemType '+item.type+' row flex-row justify-content-start col">\n' +
            '                                        <div class="form-check col col-md-8">\n' +
            '                                            <input '+checked+' type="checkbox" name="checkbox" class="form-check-input">\n' +
            '                                            <label class="form-check-label">'+item.name+'</label>\n' +
            '                                        </div>\n' +
            '                                    </div>\n' +
            '                                    <div class="operations col-5 col-md-4 text-right">\n' +
            '                                        <label class="change-type badge badge-secondary">'+item.type+'</label>\n' +
            '                                        <span class="del-item fa fa-minus-circle fa-lg ml-md-4 mr-2"></span>\n' +
            '                                        <span class="add-item-below fa fa-plus-circle fa-lg"></span>\n' +
            '                                    </div>\n' +
            '                                </li>';
        return itemHtml;
    }

    function renderInputItem(){
        var itemHtml = '<li class="d-flex justify-content-between align-items-center mb-4">\n' +
            '                                    <div class="item row flex-row justify-content-start col">\n' +
            '                                        <div class="form-check col">\n' +
            '                                            <input type="text" class="form-control title">\n' +
            '                                        </div>\n' +
            '                                    </div>\n' +
            '                                    <div class="operations col-4">\n' +
            '                                        <label class="del-insert-item badge badge-secondary">X</label>\n' +
            '                                        <label class="save-insert-item badge badge-primary ml-2">Save</label>\n' +
            '                                    </div>\n' +
            '                                </li>';
        return itemHtml
    }

    function renderDbclickItem(name) {
        var itemHtml = '<li class="d-flex justify-content-between align-items-center mb-4">\n' +
            '                                    <div class="item row flex-row justify-content-start col">\n' +
            '                                        <div class="form-check col">\n' +
            '                                            <input type="text" value="'+name+'" class="form-control title">\n' +
            '                                        </div>\n' +
            '                                    </div>\n' +
            '                                    <div class="operations col-3">\n' +
            '                                        <label class="save-insert-item badge badge-primary">Save</label>\n' +
            '                                    </div>\n' +
            '                                </li>'
        return itemHtml
    }

    function setItem(name){
        return {name:name, type:'item', checked:false}
    }
});